import re
import codecs
import timeit

def findPattern(pattern):
    start = timeit.default_timer()
    pattern = re.compile(pattern)
    matches = pattern.findall(fileContent)
    stop = timeit.default_timer()
    time = round(stop - start, 6)
    return time, len(matches)

# Getting the file content and storing it in fileContent
file = codecs.open("article.html", "r", "utf-8")
fileContent = file.read()

# Parallel lists that contain patterns and their names
names = ['E-mail',
         'Header',
         "Website ending with .info"]
patterns = [r'(?<=\s)\S+@[A-Za-z]+\.[A-Za-z]+(\.[a-zA-Z]+)?',
            r'(?<=mw-headline" id=")\w+(?=">)',
            r'https://(\S+\.?\S+)(\.info)(/\S+)+(?=")']

# Printing number of matches and executaion time for all patterns
for i in range(len(names)):
    time, matches = findPattern(patterns[i])
    print('Pattern Name:', names[i])
    print("Program Executed in "+ str(time))
    print("Number of Matches = ", matches, end='\n\n')
